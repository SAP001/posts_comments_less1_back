package main

import (
	"encoding/json"
	"fmt"
	"gitlab.com/posts_blogs_less/models"
	"net/http"

	repo "gitlab.com/posts_blogs_less/repo/mock"
)

func hello(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "hello")
}

func posts(w http.ResponseWriter, r *http.Request) {
	posts := repo.Repo.Posts()
	for i := range posts {
		posts[i].Comments = repo.Repo.CommentsByPost(posts[i].ID)
	}
	b, _ := json.Marshal(posts)
	w.Write(b)
}

func delPost(w http.ResponseWriter, r *http.Request) {
	var postID int
	err := json.NewDecoder(r.Body).Decode(&postID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	repo.Repo.DelPost(postID)
	w.WriteHeader(http.StatusOK)
}

func createPost(w http.ResponseWriter, r *http.Request) {
	post := models.Post{}
	err := json.NewDecoder(r.Body).Decode(&post)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	id := repo.Repo.CreatePost(&post)
	b, _ := json.Marshal(&id)
	w.Write(b)
}

func createComment(w http.ResponseWriter, r *http.Request) {
	comment := &models.Comment{}
	err := json.NewDecoder(r.Body).Decode(comment)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	id := repo.Repo.CreateComment(comment)
	b, _ := json.Marshal(&id)
	w.Write(b)
}

func main() {
	http.HandleFunc("/hello", hello)

	http.HandleFunc("/posts", posts)
	http.HandleFunc("/del_post", delPost)
	http.HandleFunc("/create_post", createPost)

	http.HandleFunc("/create_comment", createComment)

	http.ListenAndServe(":8080", nil)
}
